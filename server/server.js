require('./config/config')

const express = require('express');
const mongoose = require('mongoose');
const path = require('path');

const app = express();
const bodyParser = require("body-parser");


// parse application/json
app.use(bodyParser.json());

app.use(express.static(path.resolve(__dirname, '../public')));


// Global routes
app.use(require('./routes/index'));

mongoose.connect(process.env.URLBD, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
}, (err, res) => {
    if (err) throw err;

    console.log('BD connected!!')
});

app.listen(process.env.PORT, () => {
    console.log('listen port 3000');
});
