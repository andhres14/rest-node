const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');

const Schema = mongoose.Schema;

let categoriesSchema = new Schema({
    name: {
        type: String,
        unique: true,

        required: [true, 'Name is required'],
    },
    id_user: {
        type: Schema.Types.ObjectId,
        ref: 'User'
    },
});

//categoriesSchema.plugin(uniqueValidator, { message: '{PATH} must be unique' })

module.exports = mongoose.model('Category', categoriesSchema);
