const mongoose = require('mongoose');
const uniqueValidator = require('mongoose-unique-validator');


let rolesOk = {
    values: ['ADMIN_ROLE', 'USER_ROLE'],
    message: '{VALUE} is a invalid role'
}

let Schema = mongoose.Schema;

let userSchema = new Schema({
    name: {
        type: String,
        required: [true, 'Name is required'],
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'Email is required']
    },
    password: {
        type: String,
        required: [true, 'Password is required']
    },
    img: {
        type: String,
        required: false
    },
    role: {
        type: String,
        default: 'USER_ROLE',
        enum: rolesOk
    },
    status: {
        type: Boolean,
        default: true
    },
    google: {
        type: Boolean,
        default: false
    }
});

userSchema.methods.toJSON = function () {
    let user2 = this;
    let userObject = user2.toObject();
    delete userObject.password;

    return userObject;
};

userSchema.plugin(uniqueValidator, { message: '{PATH} must be unique' })


module.exports = mongoose.model('User', userSchema);
