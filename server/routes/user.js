const express = require('express');
const app = express();
const bcrypt = require('bcrypt');
const _ = require('underscore');

const User = require('../models/user');
const { checkToken, checkAdminRole } = require('../middlewares/auth')

app.get('/users', [checkToken], function (req, res) {
    let _from = Number(req.query.from || 0);
    let limit = Number(req.query.limit || 5);

    User.find({status: true}, 'name email role status google img')
        .skip(_from)
        .limit(limit)
        .exec((err, usersBD) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    err
                });
            }

            User.countDocuments({status: true}, (err, countUsers) => {
                res.json({
                    success: true,
                    users: usersBD,
                    count: countUsers
                });
            });
        });
});

app.post('/users', [checkToken, checkAdminRole], function (req, res) {
    let payload = req.body;

    let user = new User({
        name: payload.name,
        email: payload.email,
        password: bcrypt.hashSync(payload.password.toString(), 10),
        role: payload.role
    });

    user.save((err, userBD) => {
        if (err) {
            return res.status(400).json({
                success: false,
                err
            });
        }

        res.status(201).json({
            success: true,
            user: userBD
        });
    });
});

app.put('/users/:id', [checkToken, checkAdminRole], function (req, res) {
    let id = req.params.id;
    let payload = _.pick(req.body, ['name', 'email', 'img', 'role', 'status']);

    User.findByIdAndUpdate(id, payload, {
        new: true,
        runValidators: true,
        context: 'query'
    }, (err, userBD) => {
        if (err) {
            return res.status(400).json({
                success: false,
                err
            });
        }

        res.json({
            success: true,
            user: userBD
        });
    });

});

app.delete('/users/:id', [checkToken, checkAdminRole], function (req, res) {
    let id = req.params.id;
    User.findByIdAndUpdate(id, {status: false}, {
        new: true,
        context: 'query'
    }, (err, userDeleted) => {
        if (err) {
            return res.status(400).json({
                success: false,
                err
            });
        }

        if (!userDeleted) {
            return res.status(400).json({
                success: false,
                err: {
                    message: "Usuario no encontrado"
                }
            });
        }


        res.json({
            success: true,
            user: userDeleted
        });

    });
});

module.exports = app;
