const express = require('express');
const app = express();
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const _ = require('underscore');

const {OAuth2Client} = require('google-auth-library');
const client = new OAuth2Client(process.env.CLIENT_ID);

const User = require('../models/user');

app.post('/sign-in', (req, res) => {

    let payload = _.pick(req.body, ['email', 'password']);

    User.findOne({email: payload.email}, (err, userBD) => {
        if (err) {
            return res.status(500).json({
                success: false,
                err
            });
        }

        if (!userBD || !bcrypt.compareSync(payload.password, userBD.password)) {
            res.status(400).json({
                success: false,
                err: {
                    message: 'User or pass invalid'
                }
            });
        }

        let jwToken = jwt.sign({
            user: userBD
        }, process.env.SEED, {expiresIn: process.env.EXPIRATION_TOKEN});

        res.json({
            success: true,
            user: userBD,
            token: jwToken
        })


    });

});

// Config google
async function verify(token) {
    const ticket = await client.verifyIdToken({
        idToken: token,
        audience: process.env.CLIENT_ID,
    });
    const payload = ticket.getPayload();
    return {
        name: payload.name,
        email: payload.email,
        img: payload.picture,
        google: true
    }
}


app.post('/google', async (req, res) => {
    let token = req.body.token;
    let googleUser = await verify(token)
        .catch(e => {
            res.status(403).json({
                status: false,
                err: e
            });
        });
    User.findOne({email: googleUser.email}, (err, userBD) => {
        if (err) {
            return res.status(500).json({
                success: false,
                err
            });
        }

        if (userBD) {
            if (!userBD.google) {
                return res.status(400).json({
                    success: false,
                    err: {
                        success: false,
                        message: "Google user auth not valid",
                    }
                });
            } else {
                let jwToken = jwt.sign({
                    user: userBD
                }, process.env.SEED, {expiresIn: process.env.EXPIRATION_TOKEN});

                res.json({
                    success: true,
                    user: userBD,
                    token: jwToken
                });
            }
        } else {
            //user not exist
            let newUser = new User();
            newUser.name = googleUser.name;
            newUser.email = googleUser.email;
            newUser.img = googleUser.img;
            newUser.google = true;
            newUser.password = '=)';

            newUser.save((err, userBD) => {
                if (err) {
                    return res.status(500).json({
                        success: false,
                        err
                    });
                }

                let jwToken = jwt.sign({
                    user: userBD
                }, process.env.SEED, {expiresIn: process.env.EXPIRATION_TOKEN});

                res.json({
                    success: true,
                    user: userBD,
                    token: jwToken
                });

            });
        }
    });
});

module.exports = app;
