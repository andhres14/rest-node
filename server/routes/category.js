const express = require('express');
const _ = require('underscore');

let { checkToken, checkAdminRole } = require('../middlewares/auth')

let app = express();

let Category = require('../models/category')


app.get('/categories', [checkToken], function (req, res) {
    let _from = Number(req.query.from || 0);
    let limit = Number(req.query.limit || 5);

    Category.find({})
        .sort('name')
        .populate('id_user', 'email name')
        .skip(_from)
        .limit(limit)
        .exec((err, categoriesBD) => {
            if (err) {
                return res.status(400).json({
                    success: false,
                    err
                });
            }

            Category.countDocuments({}, (err, countCategories) => {
                res.json({
                    success: true,
                    categories: categoriesBD,
                    count: countCategories
                });
            });
        });
});

app.get('/categories/:id', [checkToken], function (req, res) {
    let categoryId = req.params.id;
    Category.findOne({_id: categoryId}, 'name',(err, categoryBD) => {
            if (err) {
                return res.status(404).json({
                    success: false,
                    err
                });
            }

            res.json({
                success: true,
                category: categoryBD
            });
        });
});

app.post('/categories', [checkToken], function (req, res) {
    let payload = req.body;

    let category = new Category({
        name: payload.name,
        id_user: req.user._id
    });

    category.save((err, categoryBD) => {
        if (err) {
            return res.status(400).json({
                success: false,
                err
            });
        }

        res.status(201).json({
            success: true,
            category: categoryBD
        });
    });
});

app.put('/categories/:id', [checkToken], function (req, res) {
    let id = req.params.id;
    let payload = _.pick(req.body, ['name']);

    Category.findByIdAndUpdate(id, payload, {
        new: true,
        runValidators: true,
        context: 'query'
    }, (err, categoryBD) => {
        if (err) {
            return res.status(400).json({
                success: false,
                err
            });
        }

        if (!categoryBD) {
            return res.status(404).json({
                success: false,
                err: {
                    message: "Categoria no encontrada"
                }
            });
        }


        res.json({
            success: true,
            category: categoryBD
        });
    });

});

app.delete('/categories/:id', [checkToken, checkAdminRole], function (req, res) {
    let id = req.params.id;
    Category.findByIdAndRemove(id, {new: true}, (err, categoryDeleted) => {
        if (err) {
            return res.status(400).json({
                success: false,
                err
            });
        }

        if (!categoryDeleted) {
            return res.status(404).json({
                success: false,
                err: {
                    message: "Categoria no encontrada"
                }
            });
        }

        res.json({
            success: true,
            category: null
        });

    });
});


module.exports = app;
