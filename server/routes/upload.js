const express = require('express');
const fileUpload = require('express-fileupload');
const app = express();

app.use(fileUpload({
    useTempFiles : true,
    tempFileDir : '/tmp/'
}));

app.put('/uploads', (req, res) => {
    if (!req.files) {
        return res.status(400).json({
            success: false,
            err: {
                message: "File not selected"
            }
        })
    }

    let selectedFile = req.files.archivo;
    let filename = selectedFile.name.split('.');
    let extension = filename[filename.length - 1];

    //Extensions available
    let availableExtensions = ['png', 'jpg', 'gif', 'jpeg'];

    if (availableExtensions.indexOf(extension) < 0) {
        return res.status(500).json({
            success: false,
            err: {
                message: "Invalid extension"
            }
        });
    }

    selectedFile.mv('uploads/filename.jpg', (err) => {
        if (err) {
            return res.status(500).json({
                success: false,
                err
            });
        }
        res.json({
            success: true,
            message: "Image uploaded!!"
        });
    });

});

module.exports = app;
