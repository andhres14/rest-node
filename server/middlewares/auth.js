const jwt = require('jsonwebtoken');

// =========== CHECK TOKEN ===========
let checkToken = (req, res, next) => {
    let jwToken = req.get('Authorization');

    jwt.verify(jwToken, process.env.SEED, (err, decoded) => {
        if (err) {
            if (err) {
                return res.status(401).json({
                    success: false,
                    err: {
                        message: "Token no válido"
                    }
                });
            }
        }
        req.user = decoded.user;
        next();

    });
};

// =========== CHECK TOKEN ===========
let checkAdminRole = (req, res, next) => {
    let jwToken = req.get('Authorization');

    jwt.verify(jwToken, process.env.SEED, (err, decoded) => {
        if (err) {
            if (err) {
                return res.status(401).json({
                    success: false,
                    err: {
                        message: "Invalid token"
                    }
                });
            }
        }
        req.user = decoded.user;
        if (req.user.role !== 'ADMIN_ROLE') {
            return res.status(401).json({
                success: false,
                err: {
                    message: "Admin role required!!"
                }
            });
        }

        next();

    });
};

module.exports = {
    checkToken,
    checkAdminRole
};
