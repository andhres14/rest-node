// ============ PORT ===============
process.env.PORT = process.env.PORT || 3000;
// ============ ENTORNO ===============
process.env.NODE_ENV = process.env.NODE_ENV || 'dev';
// ============ TOKEN EXPIRATION ===============
process.env.EXPIRATION_TOKEN = '48h';
// ============ AUTH SEED ===============
process.env.SEED = process.env.SEED || 'secret-dev';
// ============ BD ===============
process.env.URLBD = (process.env.NODE_ENV === 'dev') ? 'mongodb://localhost:27017/coffee' : process.env.MONGO_URI;

process.env.CLIENT_ID = process.env.CLIENT_ID || '217543604247-q5ttth5jhi8rovq82bpl5sseaepbt4ck.apps.googleusercontent.com';
